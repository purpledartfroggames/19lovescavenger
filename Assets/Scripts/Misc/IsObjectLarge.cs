﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsObjectLarge : MonoBehaviour
{ 
    [SerializeField]
    float CurObjectSizeFactor;

    public float LargeObjectFactor()
    {
        return Mathf.Clamp(CurObjectSizeFactor, 1f, 3f);
    }

}
