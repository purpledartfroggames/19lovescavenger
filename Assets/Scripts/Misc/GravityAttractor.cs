﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityAttractor : MonoBehaviour
{
    [SerializeField]
    float GravityRange;
    [SerializeField]
    float GravityImpact;

    // Update is called once per frame
    void Update()
    {
        Vector2 offset = Vector2.zero;
        GameObject PlayerGO = GameObject.FindGameObjectWithTag("Player");

        if (PlayerGO == null)
            return;

        if (Vector2.Distance(this.transform.position, PlayerGO.transform.position) < GravityRange)
        {
            offset = this.transform.position - PlayerGO.transform.position;

            PlayerGO.GetComponent<Rigidbody2D>().AddForce(offset.normalized * GravityImpact);
        }
    }
}
