﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapTimer : MonoBehaviour
{
    [SerializeField]
    int ChanceOfHundred;
    [SerializeField]
    float TimeToCollectBy;
    [SerializeField]
    GameObject DepleteFXPrefab;
    [SerializeField]
    GameObject DepleteFXPrefabNoCollision;

    bool TimedScrap;

    // Start is called before the first frame update
    void Start()
    {
        TimedScrap = Random.Range(0, 100) < ChanceOfHundred;
        
        if (TimedScrap == true)
        {
            Invoke("KillScrap", TimeToCollectBy);
        }
    }

    void Update()
    {
        if (TimeManager.Instance.CurTimeLeftOnLevel() <= 0f)
        {
            this.KillScrap();
        }
    }

    public bool IsTimedScrap()
    {
        return TimedScrap;
    }

    private void KillScrap()
    {
        if (LevelManager.Instance.CurCollectablesPool().childCount <= 1)
        {
            PlayerManager.Instance.GetPlayerGORigibody().velocity = new Vector2(0f, 0f);

            if (LevelManager.Instance.CurLevel() == LevelManager.Instance.CurMaxLevel())
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
                LevelManager.Instance.EndGame();
                return;
            }
            else
            {
                LevelManager.Instance.NextLevel();
            }
        }

        GameObject depleteFX;

        if (GameConfig.HardcoreMode == true)
            depleteFX = Instantiate(DepleteFXPrefab);
        else
            depleteFX = Instantiate(DepleteFXPrefabNoCollision);

        if (depleteFX != null)
        {
            depleteFX.transform.position = this.gameObject.transform.position;

            if (depleteFX.GetComponent<ParticleSystem>().isPlaying == false)
                depleteFX.GetComponent<ParticleSystem>().Play();
        }

        AudioManager.Instance.PlaySoundInteraction("Implode");
        PlayerManager.Instance.AdjustPoints(Mathf.Clamp(-this.gameObject.GetComponent<Points>().collectPoints(), -1000, -10));
        StorageManager.Instance.AdjustNumOfPollution(1);

        Destroy(this.gameObject);
    }
}
