﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    [SerializeField]
    GameObject CollisionFX;

    public void DoKillPlayer()
    {
        GameObject collisionFX = Instantiate(CollisionFX);     

        if (collisionFX != null)
        {
            collisionFX.transform.position = this.gameObject.transform.position;

            if (collisionFX.GetComponent<ParticleSystem>().isPlaying == false)
                collisionFX.GetComponent<ParticleSystem>().Play();
        }

        Destroy(this.gameObject);
    }
}
