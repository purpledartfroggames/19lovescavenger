﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBlinker : MonoBehaviour
{
    [SerializeField]
    float BlinkSpeed;
    [SerializeField]
    Image MyImage;
    [SerializeField]
    string TopIndicator;

    float LastExecute = 0f;
    bool isOn;

    // Start is called before the first frame update
    void Update()
    {
        if (PlayerPrefs.GetInt(TopIndicator, 0) == 1)
        {
            if (Time.unscaledTime > LastExecute + BlinkSpeed)
            {
                LastExecute = Time.unscaledTime;
                isOn = !isOn;

                if (isOn == true)
                    MyImage.color = new Color(1, 0, 0, 1);
                else
                    MyImage.color = new Color(1, 0, 0, 0);
            }
        }
        else
        {
            MyImage.color = new Color(.5f, 0, 0, .1f);
        }
    }
}
