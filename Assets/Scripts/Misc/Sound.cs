﻿using UnityEngine;
using UnityEngine.Audio;

[System.Serializable] //Ensures assign variable
public class Sound
{

    public string name;
    public AudioClip clip;
    public bool loop;

    [Range(0f, 2f)]
    public float volume;

    [Range(.1f, 3f)]
    public float pitch;

    [HideInInspector]
    public AudioSource source;
}
