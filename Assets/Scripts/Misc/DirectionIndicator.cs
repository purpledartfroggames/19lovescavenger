﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionIndicator : MonoBehaviour
{
    [SerializeField]
    GameObject MyIndicator;

    GameObject MyPlayerGO;
    Transform MyPlayer;

    [SerializeField]
    SpriteRenderer MySpriteRenderer;

    LeikaController MyLeikaController;
    bool hasLeika;

    // Start is called before the first frame update
    void Start()
    {
        MyPlayerGO = GameObject.FindGameObjectWithTag("Player");
        MyPlayer = MyPlayerGO.transform;

        InvokeRepeating("Blink", .5f, .5f);
    }

    // Update is called once per frame
    void Update()
    {
        this.Leika();

        if (MyPlayerGO == null)
        {
            MyPlayerGO = GameObject.FindGameObjectWithTag("Player");
            
        }
        if (MyPlayerGO == null)
        {
            MyIndicator.transform.position = this.transform.position;
        }
        else
        {
            Vector2 IndicatorPosition;

            MyPlayer = MyPlayerGO.transform;
            IndicatorPosition = this.transform.position - MyPlayer.position;
            MyIndicator.transform.position = (Vector2)MyPlayer.position + IndicatorPosition.normalized;
        }
    }

    private void Leika()
    {
        if (hasLeika == false)
        {
            if (MyLeikaController == null)
                MyLeikaController = this.GetComponent<LeikaController>();

            if (MyLeikaController != null)
                hasLeika = MyLeikaController.HasLeika();

            if (hasLeika == true)
                MySpriteRenderer.color = Color.green;
        }
    }

    private void Blink()
    {
        if (this.GetComponent<ScrapTimer>().IsTimedScrap() == true)
        {
            MySpriteRenderer.enabled = !MySpriteRenderer.enabled;
        }
    }
}
