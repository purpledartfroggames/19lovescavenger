﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnStart : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (TimeManager.Instance.IsPaused() == false)
            Destroy(this.gameObject);
    }
}
