﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleBrother : MonoBehaviour
{
    [SerializeField]
    GameObject Brother;

    public void SetMyBrother(GameObject NewBrother)
    {
        Brother = NewBrother;
    }

    public GameObject MyBrother()
    {
        return Brother;
    }
}
