﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour
{
    [SerializeField]
    float BlinkSpeed;
    [SerializeField]
    bool UseTimeLeft;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("BlinkGroup", 0f, BlinkSpeed);
    }

    private void BlinkGroup()
    {
        if (UseTimeLeft == false || TimeManager.Instance.CurTimeLeftOnLevel() < 10)
            this.gameObject.SetActive(!this.gameObject.activeSelf);
    }
}
