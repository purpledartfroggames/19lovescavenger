﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleTeleport : MonoBehaviour
{
    [SerializeField]
    float TeleportInterval;

    float TimeOfLastTeleport;

    // Start is called before the first frame update
    void Start()
    {
        TimeOfLastTeleport = 0f;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "DangerBlackHole")
        {
            if (TimeOfLastTeleport == 0f
            ||  Time.time > (TimeOfLastTeleport + TeleportInterval))
            {
                this.DoTeleport(collision);
            }
        }
    }

    private void DoTeleport(Collider2D collision)
    {
        this.gameObject.transform.position = collision.gameObject.GetComponent<BlackHoleBrother>().MyBrother().transform.position;
        TimeOfLastTeleport = Time.time;
    }
}
