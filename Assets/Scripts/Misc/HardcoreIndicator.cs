﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HardcoreIndicator : MonoBehaviour
{
    [SerializeField]
    Image MyImage;

    // Start is called before the first frame update
    void Start()
    {
        MyImage.enabled = GameConfig.HardcoreMode;
    }
}
