﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour
{
    [SerializeField]
    float MaxPoints;

    string PointText = "";

    public int collectPoints()
    {
        int points = Mathf.RoundToInt(MaxPoints - (TimeManager.Instance.CurTimeOnLevel() - TimeManager.Instance.CurTimeLeftOnLevel()));


        if (GameConfig.HardcoreMode == true)
        {
            points = points * 2;
            PointText = "Hardcore ";
        }

        if (this.GetComponent<ScrapTimer>().IsTimedScrap() == true)
        {
            points = points * 2;
            PointText += "Timed Scrap ";
        }

        GameMessageBoxManager.Instance.TimedMessage(PointText + " " + points.ToString(), .5f, false, Color.yellow);

        return points;
    }
}
