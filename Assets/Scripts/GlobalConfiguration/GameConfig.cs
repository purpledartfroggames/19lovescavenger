﻿public static class GameConfig
{
    private static bool playMusic, hardcoreMode, playAsJenny, playAsIgor, playAsLeika, foundLeika;

    private static int top1Score, top2Score, top3Score;

    #region bool
    public static bool PlayMusic
    {
        get
        {
            return playMusic;
        }
        set
        {
            playMusic = value;
        }
    }

    public static bool HardcoreMode
    {
        get
        {
            return hardcoreMode;
        }
        set
        {
            hardcoreMode = value;
        }
    }

    public static bool PlayAsJenny
    {
        get
        {
            return playAsJenny;
        }
        set
        {
            playAsJenny = value;
        }
    }

    public static bool PlayAsIgor
    {
        get
        {
            return playAsIgor;
        }
        set
        {
            playAsIgor = value;
        }
    }

    public static bool PlayAsLeika
    {
        get
        {
            return playAsLeika;
        }
        set
        {
            playAsLeika = value;
        }
    }

    public static bool FoundLeika
    {
        get
        {
            return foundLeika;
        }
        set
        {
            foundLeika = value;
        }
    }
    #endregion

    #region int
    public static int Top1Score
    {
        get
        {
            return top1Score;
        }
        set
        {
            top1Score = value;
        }
    }

    public static int Top2Score
    {
        get
        {
            return top2Score;
        }
        set
        {
            top2Score = value;
        }
    }

    public static int Top3Score
    {
        get
        {
            return top3Score;
        }
        set
        {
            top3Score = value;
        }
    }
    #endregion
}