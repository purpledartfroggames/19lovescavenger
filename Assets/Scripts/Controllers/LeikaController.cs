﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeikaController : MonoBehaviour
{
    [SerializeField]
    int PctChanceOfLeika;

    bool hasLeika;

    // Start is called before the first frame update
    void Start()
    {
        hasLeika = (Random.Range(0, 100) < PctChanceOfLeika) == true ? true : false;
    }

    public bool HasLeika()
    {
        return hasLeika;
    }
}
