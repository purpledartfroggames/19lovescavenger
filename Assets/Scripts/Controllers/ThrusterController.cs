﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrusterController : MonoBehaviour
{
    [SerializeField]
    ParticleSystem myParticleSystem;

    [SerializeField]
    float KeepAliveTime;
    float KeepAliveUntil;

    private void Update()
    {
        if (Time.time > KeepAliveUntil
        &&  KeepAliveTime != 0f )
        {
            myParticleSystem.Stop();
        }
    }

    public void TriggerThruster()
    {
        KeepAliveUntil = Time.time + KeepAliveTime;

        if (myParticleSystem.isPlaying == false)
        {
            myParticleSystem.Play();
        }
    }
}
