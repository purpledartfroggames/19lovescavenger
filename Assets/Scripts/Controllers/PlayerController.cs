﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float PlayerSpeed;
    [SerializeField]
    float PlayerThrusterForce;
    [SerializeField]
    float PlayerRotationForce;
    [SerializeField]
    ThrusterController myThrusterController;

    [SerializeField]
    ThrusterController myThrusterRUController;
    [SerializeField]
    ThrusterController myThrusterRLController;
    [SerializeField]
    ThrusterController myThrusterLUController;
    [SerializeField]
    ThrusterController myThrusterLLController;

    [SerializeField]
    string PlayerName;

    private void Update()
    {
        if (TimeManager.Instance.IsPaused() == true
        && (Input.GetKeyDown(KeyCode.Space)
        ||  Input.touches.Length == 2))
        {
            TimeManager.Instance.TogglePause(false, false);
        }
    }

    void FixedUpdate()
    {
        if (Application.platform == RuntimePlatform.WindowsPlayer
        || Application.platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButton(0))
            {
                if (EventSystem.current.IsPointerOverGameObject() == false)
                {
                    this.SideThrusterScreenPosition(Input.mousePosition);
                }
            }
            else if (Input.GetKey(KeyCode.Space))
            {
                this.ForwardThruster();
            }
            else if (Input.GetKey(KeyCode.A))
            {
                this.SideThrusterFromButton(true, false);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.SideThrusterFromButton(false, true);
            }
            else if (Input.GetKey(KeyCode.M))
            {
                AudioManager.Instance.PlaySoundTheme("Theme");
            }

            if (Input.GetKeyDown(KeyCode.Space))
                this.StartThrusterSound(false);
            else if (Input.GetKeyUp(KeyCode.Space))
                this.StopThrusterSound(false);
            else if (Input.GetMouseButtonDown(0))
                this.StartThrusterSound(true);
            else if (Input.GetMouseButtonUp(0))
                this.StopThrusterSound(true);
        }

        else if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.touches.Length == 1)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId) == false)
                {
                    Vector2 touchPosition = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
                    this.SideThrusterScreenPosition(Input.touches[0].position);

                    if (Input.touches[1].phase == TouchPhase.Began)
                        this.StartThrusterSound(true);
                    else if (Input.touches[1].phase == TouchPhase.Ended)
                        this.StopThrusterSound(true);
                }
            }
            else if (Input.touches.Length >= 2)
            {
                this.ForwardThruster();

                if (Input.touches[2].phase == TouchPhase.Began)
                    this.StartThrusterSound(false);
                else if (Input.touches[2].phase == TouchPhase.Ended)
                    this.StopThrusterSound(false);
            }
        }

        this.CheckEndLevel();
    }

    private void SideThrusterScreenPosition(Vector2 _screenPosition)
    {
        float deltaRotation = 1f;
        Rigidbody2D myRigibody2D = this.GetComponent<Rigidbody2D>();

        if (_screenPosition.x < (Screen.width / 2))
        {
            deltaRotation = PlayerRotationForce;

            myThrusterLUController.TriggerThruster();
            myThrusterLLController.TriggerThruster();
        }
        else if (_screenPosition.x > (Screen.width / 2))
        {
            deltaRotation = -PlayerRotationForce;

            myThrusterRUController.TriggerThruster();
            myThrusterRLController.TriggerThruster();
        }

        myRigibody2D.MoveRotation(myRigibody2D.rotation + deltaRotation * Time.fixedDeltaTime);
    }

    private void SideThrusterFromButton(bool Left, bool Right)
    {
        float deltaRotation = 1f;
        Rigidbody2D myRigibody2D = this.GetComponent<Rigidbody2D>();

        if (Left == true && Right == false)
        {
            deltaRotation = PlayerRotationForce;

            myThrusterLUController.TriggerThruster();
            myThrusterLLController.TriggerThruster();
        }
        else if (Right == true && Left == false)
        {
            deltaRotation = -PlayerRotationForce;

            myThrusterRUController.TriggerThruster();
            myThrusterRLController.TriggerThruster();
        }

        myRigibody2D.MoveRotation(myRigibody2D.rotation + deltaRotation * Time.fixedDeltaTime);
    }

    private void ForwardThruster()
    {
        this.GetComponent<Rigidbody2D>().AddForce(transform.up * PlayerThrusterForce);
        myThrusterController.TriggerThruster();
    }

    public void CheckEndLevel()
    {
        if (LevelManager.Instance.CurCollectablesPool().childCount == 0
        && LevelManager.Instance.CurLevel() < LevelManager.Instance.CurMaxLevel())
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
            LevelManager.Instance.NextLevel();
        }
        else if (LevelManager.Instance.CurCollectablesPool().childCount == 0
        && LevelManager.Instance.CurLevel() == LevelManager.Instance.CurMaxLevel())
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
            LevelManager.Instance.EndGame();
        }
    }

    private void StartThrusterSound(bool _doLow)
    {
        AudioManager.Instance.PlaySoundInteraction(_doLow == true ? "BasicJetLow" : "BasicJet");
    }

    private void StopThrusterSound(bool _doLow)
    {
        AudioManager.Instance.StopPlaySoundInteraction(_doLow == true ? "BasicJetLow" : "BasicJet");
    }

    private void OnDestroy()
    {
        this.StopThrusterSound(false);
        this.StopThrusterSound(true);

        if (HighScoreManager.Instance != null)
        {
            HighScoreManager.Instance.EndGameScoreInput(PlayerManager.Instance.CurPoints(), false);
        }


        if (AudioManager.Instance == null)
            return;

        AudioManager.Instance.PlaySoundInteraction("ShipCrash");
    }

    public string GetPlayerName()
    {
        return PlayerName;
    }
}
