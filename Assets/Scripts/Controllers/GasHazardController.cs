﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasHazardController : MonoBehaviour
{
    [SerializeField]
    float TimeBuffer;

    float TimeToCheck = 0;
    bool InGas = false;

    Collider2D CurCollision;

    // Update is called once per frame
    void Update()
    {
        if (InGas == true
        &&  TimeToCheck != 0f 
        &&  Time.time > TimeToCheck)
        {
            this.gameObject.GetComponent<KillPlayer>().DoKillPlayer(); 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "DangerGas")
        {
            InGas = true;
            TimeToCheck = Time.time + TimeBuffer;
            CurCollision = collision;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "DangerGas")
        {
            InGas = false;
            TimeToCheck = 0f;
        }
    }

    public int TimeLeftInGas()
    {
        return (int) (TimeToCheck - Time.time);
    }

    public bool IsInGas()
    {
        return InGas;
    }
}
