﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollecterController : MonoBehaviour
{
    [SerializeField]
    GameObject CollectFXEngine;
    [SerializeField]
    GameObject CollectFXRocketSegment;
    [SerializeField]
    GameObject CollectFXSolarPanel;
    [SerializeField]
    GameObject CollectFXSateliteDish;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collectFX = null;

        if (collision.gameObject.tag == "CollectableEngine")
        {
            StorageManager.Instance.AdjustNumOfEngines(1);
            collectFX = Instantiate(CollectFXEngine);         
        }
        else if (collision.gameObject.tag == "CollectableRocketSegment")
        {
            StorageManager.Instance.AdjustNumOfRocketSegments(1);
            collectFX = Instantiate(CollectFXRocketSegment);          

            this.Leika(collision);
        }
        else if (collision.gameObject.tag == "CollectableSolarPanel")
        {
            StorageManager.Instance.AdjustNumOfSolarPanels(1);
            collectFX = Instantiate(CollectFXSolarPanel);            
        }
        else if (collision.gameObject.tag == "CollectableSateliteDish")
        {
            StorageManager.Instance.AdjustNumOfSateliteDishes(1);
            collectFX = Instantiate(CollectFXSateliteDish);          
        }

        if (collectFX != null)
        {
            if (LevelManager.Instance.CurCollectablesPool().childCount == 1)
            {
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
                this.doTriggerNextLevel();
            }
            else if (LevelManager.Instance.CurCollectablesPool().childCount < 0)
            {
                GameStateManager.Instance.MessageError("Something wrong collect pool below 0");
            }

            PlayerManager.Instance.AdjustPoints(collision.gameObject.GetComponent<Points>().collectPoints());

            collectFX.transform.position = collision.gameObject.transform.position;
            collectFX.transform.SetParent(this.gameObject.transform);
            Destroy(collision.gameObject);

            AudioManager.Instance.PlaySoundInteraction("CollectScrap");
        }
    }

    private void Leika(Collider2D collision)
    {
        LeikaController leikaController = collision.gameObject.GetComponent<LeikaController>();

        if (leikaController == null)
            return;

        if (leikaController.HasLeika())
        {
            Invoke("LeikaMessage", 1f);
            AudioManager.Instance.PlaySoundInteraction("LeikaBark");
            GameConfig.FoundLeika = true;
            PlayerPrefs.SetInt("FoundLeika", 1);
            PlayerPrefs.Save();
        }
    }

    public void LeikaMessage()
    {
        GameMessageBoxManager.Instance.TimedMessage("You found Leika... Wuf wuf", 2f, false, Color.green);
    }

    public void doTriggerNextLevel()
    {
        if (LevelManager.Instance.CurLevel() < LevelManager.Instance.CurMaxLevel())
            LevelManager.Instance.NextLevel();
        else
            LevelManager.Instance.EndGame();
    }

}
