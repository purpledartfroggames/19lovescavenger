﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
    [SerializeField]
    GameObject CollisionFX;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        this.CollideDeadlyObject(collision.tag);
    }

    private void OnParticleCollision(GameObject other)
    {
        this.CollideDeadlyObject(other.tag);
    }

    private void CollideDeadlyObject(string _tag)
    {
        if (_tag == "Danger")
        {
            StorageManager.Instance.AdjustNumOfEngines(-StorageManager.Instance.CurNumOfEngines());
            StorageManager.Instance.AdjustNumOfRocketSegments(-StorageManager.Instance.CurNumOfRocketSegments());
            StorageManager.Instance.AdjustNumOfSateliteDishes(-StorageManager.Instance.CurNumOfSateliteDishes());
            StorageManager.Instance.AdjustNumOfSolarPanels(-StorageManager.Instance.CurNumOfSolarPanels());

            this.gameObject.GetComponent<KillPlayer>().DoKillPlayer();
        }
    }
}
