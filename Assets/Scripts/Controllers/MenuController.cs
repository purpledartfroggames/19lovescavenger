﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI CollectableEngineText;
    [SerializeField]
    TextMeshProUGUI CollectableRocketSegmentText;
    [SerializeField]
    TextMeshProUGUI CollectableSateliteDishText;
    [SerializeField]
    TextMeshProUGUI CollectableSolarPanelText;
    [SerializeField]
    TextMeshProUGUI PollutionText;
    [SerializeField]
    Image PollutionImage;

    [SerializeField]
    TextMeshProUGUI TimerCountTxt;
    [SerializeField]
    TextMeshProUGUI GasTimerTxt;
    [SerializeField]
    TextMeshProUGUI GasTimerCountTxt;

    [SerializeField]
    TextMeshProUGUI PointsText;
    [SerializeField]
    TextMeshProUGUI HeartsText;
    [SerializeField]
    TextMeshProUGUI LevelText;

    [SerializeField]
    Image LeikaActive;

    private void Update()
    {
        this.UpdateTimer();
        this.UpdateGasTimer();
        
        PointsText.text = PlayerManager.Instance.CurPoints().ToString();
        HeartsText.text = PlayerManager.Instance.CurHearts().ToString();
        LevelText.text = LevelManager.Instance.CurLevel().ToString() + " of " + LevelManager.Instance.CurMaxLevel().ToString();
        LeikaActive.color = GameConfig.FoundLeika == true ? Color.green : Color.red;
    }

    private void UpdateTimer()
    {
        int secondsInTotal = Mathf.Clamp(TimeManager.Instance.CurTimeLeftOnLevel(), 0, 1000);

        int minutes = Mathf.FloorToInt(secondsInTotal / 60F);
        int seconds = Mathf.FloorToInt(secondsInTotal - minutes * 60);
        string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);

        TimerCountTxt.text = niceTime;
    }

    private void UpdateGasTimer()
    {
        //TODO: MaKE more efficient!!!!

        GameObject MyPlayer;
        GasHazardController MyGasHazard;

        MyPlayer = GameObject.FindGameObjectWithTag("Player");

        if (MyPlayer == null)
            return;

        MyGasHazard = MyPlayer.GetComponent<GasHazardController>();

        if (MyGasHazard == null)
            return;

        if (MyGasHazard.IsInGas() == false)
        {
            GasTimerTxt.text = "";
            GasTimerCountTxt.text = "";
        }
        else
        {
            int secondsInTotal = MyGasHazard.TimeLeftInGas();

            int minutes = Mathf.FloorToInt(secondsInTotal / 60F);
            int seconds = Mathf.FloorToInt(secondsInTotal - minutes * 60);
            string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);

            GasTimerTxt.text = "Get out of gas:";
            GasTimerCountTxt.text = niceTime;
        }
    }

    public void UpdateInterface()
    {
        CollectableEngineText.text = StorageManager.Instance.CurNumOfEngines().ToString();
        CollectableRocketSegmentText.text = StorageManager.Instance.CurNumOfRocketSegments().ToString();
        CollectableSateliteDishText.text = StorageManager.Instance.CurNumOfSateliteDishes().ToString();
        CollectableSolarPanelText.text = StorageManager.Instance.CurNumOfSolarPanels().ToString();

        if (StorageManager.Instance.CurNumOfPollution() >= 21)
            PollutionText.text = "BYE!";
        else if (StorageManager.Instance.CurNumOfPollution() == 20)
            PollutionText.text = "MAX!";
        else
            PollutionText.text = StorageManager.Instance.CurNumOfPollution().ToString();

        Color newRed = new Color(1, 0, 0, ((float)StorageManager.Instance.CurNumOfPollution() / 20f));
        PollutionImage.color = newRed;
    }

    public void TogglePause()
    {
        TimeManager.Instance.TogglePause(false, false);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1); //Scene 1: Game Scene
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0); //Scene 0: Menu scene
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void DoRestartGame()
    {
        MainMenuController.DoStartGame();
    }
}
