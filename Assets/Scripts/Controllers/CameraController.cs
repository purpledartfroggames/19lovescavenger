﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    float CameraSpeed;

    GameObject PlayerTarget;

    [SerializeField]
    float reCaptureTolerance;

    Vector3 TargetPos;

    // Start is called before the first frame update
    void Start()
    {
        TargetPos = this.gameObject.transform.position;
        PlayerTarget = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        this.MoveOnPlayer();
    }

    void MoveOnPlayer()
    {
        if (TimeManager.Instance.IsPaused() == false)
        {
            if (PlayerTarget != null)
            {
                Vector2 Movement;
                float distanceToPlayer = Vector2.Distance(this.gameObject.transform.position, PlayerTarget.gameObject.transform.position);

                Movement = PlayerTarget.gameObject.transform.position - this.gameObject.transform.position;

                if (Mathf.Abs(Movement.x) < reCaptureTolerance && Mathf.Abs(Movement.y) < reCaptureTolerance)
                    TargetPos = Vector2.zero;
                else
                    TargetPos = new Vector2(Mathf.Lerp(0, Movement.normalized.x * CameraSpeed * distanceToPlayer, 0.8f),
                                            Mathf.Lerp(0, Movement.normalized.y * CameraSpeed * distanceToPlayer, 0.8f));

                this.GetComponent<Rigidbody2D>().AddForce(TargetPos);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(Vector2.zero);
            }

        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.zero);
        }
    }
}
