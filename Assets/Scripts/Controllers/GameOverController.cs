﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    private void OnDestroy()
    {
        GameObject MenuControllerGO = GameObject.FindGameObjectWithTag("MenuController");

        if (MenuControllerGO == null)
            return;

        TimeManager.Instance.TogglePause(true, false);
    }
}
