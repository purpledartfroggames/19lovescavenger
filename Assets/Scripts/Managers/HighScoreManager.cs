﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoreManager : MonoBehaviour
{
    public static HighScoreManager Instance;

    bool ScoreRegistered = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(Instance);
    }

    public void EndGameScoreInput(int _score, bool _won)
    {
        int curTop1 = PlayerPrefs.GetInt("Top1", 0);
        int curTop2 = PlayerPrefs.GetInt("Top2", 0);
        int curTop3 = PlayerPrefs.GetInt("Top3", 0);

        bool curTop1Won = PlayerPrefs.GetInt("Top1Won", 0) == 1 ? true : false;
        bool curTop2Won = PlayerPrefs.GetInt("Top2Won", 0) == 1 ? true : false;
        bool curTop3Won = PlayerPrefs.GetInt("Top3Won", 0) == 1 ? true : false;

        bool curTop1Hardcore = PlayerPrefs.GetInt("Top1Hardcore", 0) == 1 ? true : false;
        bool curTop2Hardcore = PlayerPrefs.GetInt("Top2Hardcore", 0) == 1 ? true : false;
        bool curTop3Hardcore = PlayerPrefs.GetInt("Top3Hardcore", 0) == 1 ? true : false;

        string curTop1Name = PlayerPrefs.GetString("Top1Name", "");
        string curTop2Name = PlayerPrefs.GetString("Top2Name", "");
        string curTop3Name = PlayerPrefs.GetString("Top3Name", "");

        if (_score != 0 && ScoreRegistered == false)
        {
            if (_score >= curTop1)
            {
                curTop3 = curTop2;
                curTop2 = curTop1;
                curTop1 = _score;
                curTop1Won = _won;
                curTop1Hardcore = GameConfig.HardcoreMode;
                curTop1Name = PlayerManager.Instance.GetPlayerName();

            }
            else if (_score >= curTop2)
            {
                curTop3 = curTop2;
                curTop2 = _score;
                curTop2Won = _won;
                curTop2Hardcore = GameConfig.HardcoreMode;
                curTop2Name = PlayerManager.Instance.GetPlayerName();
            }
            else if (_score >= curTop3)
            {
                curTop3 = _score;
                curTop3Won = _won;
                curTop3Hardcore = GameConfig.HardcoreMode;
                curTop3Name = PlayerManager.Instance.GetPlayerName();
            }

            PlayerPrefs.SetInt("Top1", curTop1);
            PlayerPrefs.SetInt("Top2", curTop2);
            PlayerPrefs.SetInt("Top3", curTop3);
            PlayerPrefs.SetInt("Top1Won", curTop1Won == true ? 1 : 0);
            PlayerPrefs.SetInt("Top2Won", curTop2Won == true ? 1 : 0);
            PlayerPrefs.SetInt("Top3Won", curTop3Won == true ? 1 : 0);
            PlayerPrefs.SetInt("Top1Hardcore", curTop1Hardcore == true ? 1 : 0);
            PlayerPrefs.SetInt("Top2Hardcore", curTop2Hardcore == true ? 1 : 0);
            PlayerPrefs.SetInt("Top3Hardcore", curTop3Hardcore == true ? 1 : 0);
            PlayerPrefs.SetString("Top1Name", curTop1Name);
            PlayerPrefs.SetString("Top2Name", curTop2Name);
            PlayerPrefs.SetString("Top3Name", curTop3Name);
            PlayerPrefs.Save();

            ScoreRegistered = true;
        }
    }

    public void SetScoreRegistered(bool _registered)
    {
        ScoreRegistered = _registered;
    }
}
