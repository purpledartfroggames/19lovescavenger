﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameMessageBoxManager : MonoBehaviour
{
    public static GameMessageBoxManager Instance;

    private float KeepMessageBoxAliveUntil;

    [SerializeField]
    GameObject MessageGroup;
    [SerializeField]
    GameObject PlayButtonGroup;

    [SerializeField]
    TextMeshProUGUI Message;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void ResetManager() //EVERY MANAGER!
    {

    }

    private void Update()
    {
        if (Time.time > KeepMessageBoxAliveUntil
        &&  KeepMessageBoxAliveUntil != 0f)
        {
            MessageGroup.SetActive(false);
            PlayButtonGroup.SetActive(false);
        }
    }

    public void TimedMessage(string _message, float _time, bool _showRestartButton, Color MyColor)
    {
        MessageGroup.SetActive(true);
        Message.color = MyColor;

        if (_showRestartButton == true)
            PlayButtonGroup.SetActive(true);
        else
            PlayButtonGroup.SetActive(false);

        Message.text = _message;
        KeepMessageBoxAliveUntil = Time.time + _time;
    }

}
