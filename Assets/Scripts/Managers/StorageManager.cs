﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : MonoBehaviour
{
    public static StorageManager Instance;

    [SerializeField]
    MenuController MyMenuController;

    int NumOfSateliteDishes;
    int NumOfEngines;
    int NumOfSolarPanels;
    int NumOfRocketSegment;
    int NumOfPollution;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void ResetManager() //EVERY MANAGER!
    {
        NumOfEngines = 0;
        NumOfRocketSegment = 0;
        NumOfSateliteDishes = 0;
        NumOfSolarPanels = 0;
        NumOfPollution = 0;
    }

    public int CurNumOfEngines()
    {
        return NumOfEngines;
    }
    public int CurNumOfRocketSegments()
    {
        return NumOfRocketSegment;
    }
    public int CurNumOfSateliteDishes()
    {
        return NumOfSateliteDishes;
    }
    public int CurNumOfSolarPanels()
    {
        return NumOfSolarPanels;
    }
    public int CurNumOfPollution()
    {
        return NumOfPollution;
    }

    public void AdjustNumOfEngines(int _adjustment)
    {
        NumOfEngines += _adjustment;
        MyMenuController.UpdateInterface();
    }
    public void AdjustNumOfRocketSegments(int _adjustment)
    {
        NumOfRocketSegment += _adjustment;
        MyMenuController.UpdateInterface();
    }
    public void AdjustNumOfSateliteDishes(int _adjustment)
    {
        NumOfSateliteDishes += _adjustment;
        MyMenuController.UpdateInterface();
    }
    public void AdjustNumOfSolarPanels(int _adjustment)
    {
        NumOfSolarPanels += _adjustment;
        MyMenuController.UpdateInterface();
    }
    public void AdjustNumOfPollution(int _adjustment)
    {
        NumOfPollution = Mathf.Clamp(NumOfPollution + _adjustment, 0, 21);
        
        MyMenuController.UpdateInterface();

        if (NumOfPollution >= 21)
            PlayerManager.Instance.GetPlayerGO().GetComponent<KillPlayer>().DoKillPlayer();
    }
}
