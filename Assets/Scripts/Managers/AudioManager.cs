﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    static public AudioManager Instance;

    public Sound[] soundsInteraction;
    public Sound[] soundsThemes;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        DontDestroyOnLoad(this.gameObject);

        foreach (Sound s in soundsInteraction)
        {
            s.source = this.gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        foreach (Sound s in soundsThemes)
        {
            s.source = this.gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        if (GameConfig.PlayMusic == true)
        {
            Instance.PlaySoundTheme("Theme");
        }
    }

    public void ResetManager() //EVERY MANAGER!
    {

    }

    public void PlaySoundInteraction(string name)
    {
        Sound s = Array.Find(soundsInteraction, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        s.source.Play();
    }

    public void StopPlaySoundInteraction(string name)
    {
        Sound s = Array.Find(soundsInteraction, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        if (s.source == null)
        {
            return;
        }

        s.source.Stop();
    }

    public void PlaySoundTheme(string name)
    {
        Sound s = Array.Find(soundsThemes, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        if (s.source == null)
        {
            return;
        }

        if (s.source.isPlaying == true)
        {
            GameMessageBoxManager.Instance.TimedMessage("Music stopped", 1f, false, Color.white);
            s.source.Stop();
        }
        else
        {
            GameMessageBoxManager.Instance.TimedMessage("Music started", 1f, false, Color.white);
            s.source.Play();
        }
    }

}
