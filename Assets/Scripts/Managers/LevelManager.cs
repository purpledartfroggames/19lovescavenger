﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    private int curLevel = 1;

    [SerializeField]
    int MaxLevel;

    //Index represents level
    [SerializeField]
    float[] LevelTime;
    [SerializeField]
    int[] LevelCollectablesCount;
    [SerializeField]
    int[] LevelDangersCount;
    [SerializeField]
    int[] LevelBlackHoleSetCount;

    [SerializeField]
    ParticleSystem LevelTransitionFX;
    [SerializeField]
    ParticleSystem EndGameFX;

    [SerializeField]
    Transform CollectablePool;
    [SerializeField]
    Transform DangersPool;

    bool GameEnded = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            this.LaunchFirstLevel();
        }
        else
        {
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void ResetManager() //EVERY MANAGER!
    {

    }

    public void LaunchFirstLevel()
    {
        ObjectSpawnManager.Instance.TriggerLevel(1);
        TimeManager.Instance.SetTimeLeftOnLevel(curLevel);
    }

    public void ClearLevel()
    {
        foreach (Transform goTransform in CollectablePool.transform)
        {
            Destroy(goTransform.gameObject);
        }

        foreach (Transform goTransform in DangersPool.transform)
        {
            Destroy(goTransform.gameObject);
        }
    }

    public void NextLevel() 
    {
        this.ClearLevel();

        curLevel++;

        ObjectSpawnManager.Instance.TriggerLevel(curLevel);
        TimeManager.Instance.SetTimeLeftOnLevel(curLevel);

        if (LevelTransitionFX.isPlaying == false)
        {
            LevelTransitionFX.Play();
        }
        AudioManager.Instance.PlaySoundInteraction("Bubles");
        PlayerManager.Instance.AdjustHearts(1);

        Invoke("LevelChangeMessage", .5f);
        Invoke("StopTransition", 2f);
    }

    private void LevelChangeMessage()
    {
        GameMessageBoxManager.Instance.TimedMessage("Level " + LevelManager.Instance.CurLevel().ToString() + " reached!", 2f, false, new Color(1, 0, 1));
    }

    public void EndGame() 
    {
        if (GameEnded == false)
        {
            if (LevelTransitionFX.isPlaying == false)
                LevelTransitionFX.Play();
            if (EndGameFX.isPlaying == false)
                EndGameFX.Play();

            this.SpawnFamily();

            AudioManager.Instance.PlaySoundInteraction("Bubles");
            PlayerManager.Instance.AdjustHearts(5);
            GameEnded = true;

            HighScoreManager.Instance.EndGameScoreInput(PlayerManager.Instance.CurPoints(), true);

            Invoke("StopTransitionEnd", 2f);
        }
    }

    private void SpawnFamily()
    {
        if (GameConfig.PlayAsJenny == false)
        {
            GameObject goPlayer = Instantiate(PlayerManager.Instance.GetJennyPrefab());
            Vector2 positionFromPlayer = PlayerManager.Instance.GetPlayerGO().transform.position;

            goPlayer.transform.parent = PlayerManager.Instance.GetPlayerGroup().transform;
            goPlayer.transform.position = new Vector2(positionFromPlayer.x + 1.2f, positionFromPlayer.y);
        }

        if (GameConfig.PlayAsIgor == false)
        {
            GameObject goPlayer = Instantiate(PlayerManager.Instance.GetIgorPrefab());
            Vector2 positionFromPlayer = PlayerManager.Instance.GetPlayerGO().transform.position;

            goPlayer.transform.parent = PlayerManager.Instance.GetPlayerGroup().transform;
            goPlayer.transform.position = new Vector2(positionFromPlayer.x + 1.2f, positionFromPlayer.y);
        }

        if (GameConfig.PlayAsLeika == false)
        {
            GameObject goPlayer = Instantiate(PlayerManager.Instance.GetLeikaPrefab());
            Vector2 positionFromPlayer = PlayerManager.Instance.GetPlayerGO().transform.position;

            goPlayer.transform.parent = PlayerManager.Instance.GetPlayerGroup().transform;
            goPlayer.transform.position = new Vector2(positionFromPlayer.x - 1.2f, positionFromPlayer.y);
        }
    }

    public int NumOfCollectablesForLevel(int level)
    {
        return LevelCollectablesCount[level];
    }

    public int NumOfDangersForLevel(int level)
    {
        return LevelDangersCount[level];
    }

    public int NumOfBlackHolesForLevel(int level)
    {
        return LevelBlackHoleSetCount[level];
    }

    public Transform CurCollectablesPool()
    {
        return CollectablePool;
    }

    public Transform CurDangersPool()
    {
        return DangersPool;
    }

    public float GetLevelTime(int idx)
    {
        return LevelTime[idx];
    }

    public int CurMaxLevel()
    {
        return MaxLevel;
    }

    public int CurLevel()
    {
        return curLevel;
    }

    private void StopTransition()
    {
        LevelTransitionFX.Clear(true);
        LevelTransitionFX.Stop();
    }

    private void StopTransitionEnd()
    {
        LevelTransitionFX.Clear(true);
        LevelTransitionFX.Stop();
        TimeManager.Instance.TogglePause(false, true);
    }
}
