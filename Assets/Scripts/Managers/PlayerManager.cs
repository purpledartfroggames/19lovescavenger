﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager Instance;

    [SerializeField]
    GameObject PlayerGO;
    [SerializeField]
    Rigidbody2D PlayerGORigibody;

    [SerializeField]
    GameObject PrefabJenny;
    [SerializeField]
    GameObject PrefabIgor;
    [SerializeField]
    GameObject PrefabLeika;
    [SerializeField]
    GameObject PlayerGroup;
    [SerializeField]
    string PlayerName;

    int Points;
    int Hearts;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Instance.ResetManager();
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void ResetManager() //EVERY MANAGER!
    {
        Points = 0;
        Hearts = 0;
    }

    public int CurPoints()
    {
        return Points;
    }

    public void AdjustPoints(int _adjustment)
    {
        Points += _adjustment;
    }

    public int CurHearts()
    {
        return Hearts;
    }

    public void AdjustHearts(int _adjustment)
    {
        Hearts += _adjustment;
    }

    public void SetPlayerGO(GameObject _playerGO)
    {
        PlayerGO = _playerGO;
        PlayerName = _playerGO.GetComponent<PlayerController>().GetPlayerName();
    }

    public void SetPlayerGORigibody(Rigidbody2D _playerGORigibody)
    {
        PlayerGORigibody = _playerGORigibody;
    }

    public GameObject GetPlayerGO()
    {
        return PlayerGO;
    }

    public Rigidbody2D GetPlayerGORigibody()
    {
        return PlayerGORigibody;
    }

    public GameObject GetJennyPrefab()
    {
        return PrefabJenny;
    }

    public GameObject GetIgorPrefab()
    {
        return PrefabIgor;
    }

    public GameObject GetLeikaPrefab()
    {
        return PrefabLeika;
    }

    public GameObject GetPlayerGroup()
    {
        return PlayerGroup;
    }

    public string GetPlayerName()
    {
        return PlayerName;
    }
}
