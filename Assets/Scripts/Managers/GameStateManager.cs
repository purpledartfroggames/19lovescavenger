﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager Instance;

    [SerializeField]
    bool DeveloperMode;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void ResetManager() //EVERY MANAGER!
    {

    }

    public void MessageInfo(string _message)
    {
        if (DeveloperMode == true)
            Debug.Log(_message);
    }

    public void MessageWarning(string _message)
    {
        if (DeveloperMode == true)
            Debug.LogWarning(_message);
    }

    public void MessageError(string _message)
    {
        if (DeveloperMode == true)
            Debug.LogError(_message);
    }
}
