﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    [SerializeField]
    float GameSpeed;
    [SerializeField]
    float PausedAtSpeed;

    float TimeOnLevel;
    float TimeLeftOnLevel;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void ResetManager() //EVERY MANAGER!
    {
        GameSpeed = 0f;
        PausedAtSpeed = 0f;
    }

    private void Update()
    {
        TimeLeftOnLevel -= Time.deltaTime;
    }

    public void TogglePause(bool showGameOverText, bool showGameWonText) //Don't show standard text if game over!
    {
        if (IsPaused())
        {
            GameSpeed = PausedAtSpeed != 0f ? PausedAtSpeed : 1f;
            PausedAtSpeed = 0f;
            GameMessageBoxManager.Instance.TimedMessage("Unpaused", 1.5f, false, Color.white);
            GameStateManager.Instance.MessageInfo("Game unpaused");
        }
        else
        {
            PausedAtSpeed = GameSpeed;
            GameSpeed = 0f;

            if (showGameOverText == true)
                GameMessageBoxManager.Instance.TimedMessage("Wanna TRY finding your one and only again...?", 10f, true, Color.red);
            else if (showGameWonText == true)
                GameMessageBoxManager.Instance.TimedMessage("Congratulations..... plastic? no longer a space issue...! Redo it..?", 10f, true, Color.yellow);
            else
                GameMessageBoxManager.Instance.TimedMessage("Pause", 10f, true, Color.white);

            GameStateManager.Instance.MessageInfo("Game paused");
        }

        SetGameSpeed();
    }

    public bool IsPaused()
    {
        if (GameSpeed == 0f)
            return true;

        return false;
    }

    public float CurGameSpeed()
    {
        return GameSpeed;
    }

    public void AdjustGameSpeed(float _adjustment)
    {
        GameSpeed += _adjustment;

        SetGameSpeed();
    }

    private void SetGameSpeed()
    {
        Time.timeScale = GameSpeed;
    }

    public void SetTimeLeftOnLevel(int curLevel)
    {
        TimeLeftOnLevel = LevelManager.Instance.GetLevelTime(curLevel);
        TimeOnLevel = TimeLeftOnLevel;
    }

    public int CurTimeLeftOnLevel()
    {
        return (int)TimeLeftOnLevel;
    }

    public int CurTimeOnLevel()
    {
        return (int)TimeOnLevel;
    }
}
