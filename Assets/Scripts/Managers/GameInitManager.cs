﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitManager : MonoBehaviour
{
    static public GameInitManager Instance;

    [SerializeField]
    GameObject PlayerGroup;

    [SerializeField]
    GameObject PrefabJenny;
    [SerializeField]
    GameObject PrefabIgor;
    [SerializeField]
    GameObject PrefabLeika; //TODO

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            Instance.InitPlayer();
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    public void InitPlayer()
    {
        GameObject goPlayer = null;

        if (GameConfig.PlayAsJenny == true)
        {
            goPlayer = Instantiate(PrefabJenny);

            goPlayer.transform.parent = PlayerGroup.transform;
            goPlayer.transform.position = PlayerGroup.transform.position;
        }

        else if (GameConfig.PlayAsIgor == true)
        {
            goPlayer = Instantiate(PrefabIgor);

            goPlayer.transform.parent = PlayerGroup.transform;
            goPlayer.transform.position = PlayerGroup.transform.position;
        }

        else if (GameConfig.PlayAsLeika == true)
        {
            goPlayer = Instantiate(PrefabLeika);

            goPlayer.transform.parent = PlayerGroup.transform;
            goPlayer.transform.position = PlayerGroup.transform.position;
        }

        if (goPlayer != null)
        {
            PlayerManager.Instance.SetPlayerGO(goPlayer.gameObject);
            PlayerManager.Instance.SetPlayerGORigibody(goPlayer.GetComponent<Rigidbody2D>());
        }

        HighScoreManager.Instance.SetScoreRegistered(false);
    }

    public void ResetManagers()
    {
        AudioManager.Instance.ResetManager();
        GameMessageBoxManager.Instance.ResetManager();
        GameStateManager.Instance.ResetManager();
        LevelManager.Instance.ResetManager();
        PlayerManager.Instance.ResetManager();
        StorageManager.Instance.ResetManager();
        TimeManager.Instance.ResetManager();
        ObjectSpawnManager.Instance.ResetManager();

    }
}
