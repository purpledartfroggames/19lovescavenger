﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    Image PlayMusicCheckBox;
    [SerializeField]
    Image HardcoreModeCheckBox;
    [SerializeField]
    Image PlayAsJennyCheckBox;
    [SerializeField]
    Image PlayAsIgorCheckBox;
    [SerializeField]
    Image PlayAsLeikaCheckBox;

    [SerializeField]
    Sprite CheckBoxChecked;
    [SerializeField]
    Sprite CheckBoxUnchecked;

    [SerializeField]
    GameObject GameMenu;
    [SerializeField]
    GameObject SettingsMenu;
    [SerializeField]
    GameObject HistoryMenu;
    [SerializeField]
    GameObject HelpMenu;
    [SerializeField]
    GameObject HighscoreMenu;

    [SerializeField]
    TextMeshProUGUI MessageBoxMenu;

    [SerializeField]
    TextMeshProUGUI Top1Text;
    [SerializeField]
    TextMeshProUGUI Top2Text;
    [SerializeField]
    TextMeshProUGUI Top3Text;
    [SerializeField]
    Image Top1Hardcore;
    [SerializeField]
    Image Top2Hardcore;
    [SerializeField]
    Image Top3Hardcore;

    [SerializeField]
    GameObject LeikaGroup;

    [SerializeField]
    Image Top1Player;
    [SerializeField]
    Image Top2Player;
    [SerializeField]
    Image Top3Player;

    [SerializeField]
    Sprite SpriteJenny;
    [SerializeField]
    Sprite SpriteIgor;
    [SerializeField]
    Sprite SpriteLeika;
    [SerializeField]
    Sprite DefaultSprite;

    private void Start()
    {
        PlayMusicCheckBox.sprite = GameConfig.PlayMusic == true ? CheckBoxChecked : CheckBoxUnchecked;
        HardcoreModeCheckBox.sprite = GameConfig.HardcoreMode == true ? CheckBoxChecked : CheckBoxUnchecked;

        if (PlayerPrefs.GetInt("FoundLeika") == 1)
        {
            GameConfig.FoundLeika = true;
            LeikaGroup.SetActive(true);
        }
        else
        {
            LeikaGroup.SetActive(false);
        }

        GameConfig.PlayMusic = PlayerPrefs.GetInt("PlayMusic") == 1 ? true : false;
        GameConfig.HardcoreMode = PlayerPrefs.GetInt("HardcoreMode") == 1 ? true : false;
        GameConfig.PlayAsJenny = PlayerPrefs.GetInt("PlayAsJenny") == 1 ? true : false;
        GameConfig.PlayAsIgor = PlayerPrefs.GetInt("PlayAsIgor") == 1 ? true : false;
        GameConfig.PlayAsLeika = PlayerPrefs.GetInt("PlayAsLeika") == 1 ? true : false;
        GameConfig.FoundLeika = PlayerPrefs.GetInt("FoundLeika") == 1 ? true : false;

        PlayMusicCheckBox.sprite = GameConfig.PlayMusic == true ? CheckBoxChecked : CheckBoxUnchecked;
        HardcoreModeCheckBox.sprite = GameConfig.HardcoreMode == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsJennyCheckBox.sprite = GameConfig.PlayAsJenny == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsIgorCheckBox.sprite = GameConfig.PlayAsIgor == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsLeikaCheckBox.sprite = GameConfig.PlayAsLeika == true ? CheckBoxChecked : CheckBoxUnchecked;

        this.SwitchToGameMenu();
    }

    private void Update()
    {
        
    }

    public void StartGame()
    {
        if (GameConfig.PlayAsJenny == true || GameConfig.PlayAsIgor == true || GameConfig.PlayAsLeika)
        {
            MainMenuController.DoStartGame();
        }
        else
        {
            MessageBoxMenu.text = "You must select character";
        }
    }

    static public void DoStartGame()
    {
        PlayerPrefs.SetInt("PlayMusic", GameConfig.PlayMusic == true ? 1 : 0);
        PlayerPrefs.SetInt("HardcoreMode", GameConfig.HardcoreMode == true ? 1 : 0);
        PlayerPrefs.SetInt("PlayAsJenny", GameConfig.PlayAsJenny == true ? 1 : 0);
        PlayerPrefs.SetInt("PlayAsIgor", GameConfig.PlayAsIgor == true ? 1 : 0);
        PlayerPrefs.SetInt("PlayAsLeika", GameConfig.PlayAsLeika == true ? 1 : 0);
        PlayerPrefs.Save();

        SceneManager.LoadScene(1); //Scene 0: Menu scene

        if (GameInitManager.Instance != null)
        {
            GameInitManager.Instance.ResetManagers();
            GameInitManager.Instance.InitPlayer();
        }


    }

    public void ToggleMusic()
    {
        GameConfig.PlayMusic = !GameConfig.PlayMusic;

        PlayMusicCheckBox.sprite = GameConfig.PlayMusic == true ? CheckBoxChecked : CheckBoxUnchecked;
    }

    public void ToggleHardcore()
    {
        GameConfig.HardcoreMode = !GameConfig.HardcoreMode;

        HardcoreModeCheckBox.sprite = GameConfig.HardcoreMode == true ? CheckBoxChecked : CheckBoxUnchecked;
    }

    public void SwitchToSettingsMenu()
    {
        GameMenu.SetActive(false);
        SettingsMenu.SetActive(true);
        HistoryMenu.SetActive(false);
        HelpMenu.SetActive(false);
        HighscoreMenu.SetActive(false);
    }

    public void SwitchToGameMenu()
    {
        GameMenu.SetActive(true);
        SettingsMenu.SetActive(false);
        HistoryMenu.SetActive(false);
        HelpMenu.SetActive(false);
        HighscoreMenu.SetActive(false);
    }

    public void SwitchToHistoryMenu()
    {
        GameMenu.SetActive(false);
        SettingsMenu.SetActive(false);
        HistoryMenu.SetActive(true);
        HelpMenu.SetActive(false);
        HighscoreMenu.SetActive(false);
    }

    public void SwitchToHelpMenu()
    {
        GameMenu.SetActive(false);
        SettingsMenu.SetActive(false);
        HistoryMenu.SetActive(false);
        HelpMenu.SetActive(true);
        HighscoreMenu.SetActive(false);
    }

    public void SwitchToHighScoreMenu()
    {
        GameMenu.SetActive(false);
        SettingsMenu.SetActive(false);
        HistoryMenu.SetActive(false);
        HelpMenu.SetActive(false);
        HighscoreMenu.SetActive(true);

        Top1Text.text = PlayerPrefs.GetInt("Top1", 0).ToString();
        Top2Text.text = PlayerPrefs.GetInt("Top2", 0).ToString();
        Top3Text.text = PlayerPrefs.GetInt("Top3", 0).ToString();

        Top1Hardcore.enabled = (PlayerPrefs.GetInt("Top1Hardcore", 0) == 1) ? true : false;
        Top2Hardcore.enabled = (PlayerPrefs.GetInt("Top2Hardcore", 0) == 1) ? true : false;
        Top3Hardcore.enabled = (PlayerPrefs.GetInt("Top3Hardcore", 0) == 1) ? true : false;

        this.setPlayerNameImage(1, PlayerPrefs.GetString("Top1Name", ""));
        this.setPlayerNameImage(2, PlayerPrefs.GetString("Top2Name", ""));
        this.setPlayerNameImage(3, PlayerPrefs.GetString("Top3Name", ""));
    }

    public void setPlayerNameImage(int number, string name)
    {
        Sprite MySprite = DefaultSprite;
        bool SpriteSet = false;

        if (name == "Jenny")
        {
            MySprite = SpriteJenny;
            SpriteSet = true;
        }
        else if (name == "Igor")
        {
            MySprite = SpriteIgor;
            SpriteSet = true;
        }
        else if (name == "Leika")
        {
            MySprite = SpriteLeika;
            SpriteSet = true;
        }

        if (SpriteSet == true)
        {
            switch (number)
            {
                case 1: Top1Player.sprite = MySprite; break;
                case 2: Top2Player.sprite = MySprite; break;
                case 3: Top3Player.sprite = MySprite; break;
            }
        }
    }

    public void PlayAsJenny()
    {
        GameConfig.PlayAsJenny = !GameConfig.PlayAsJenny;

        if (GameConfig.PlayAsJenny == true)
        {
            GameConfig.PlayAsIgor = false;
            GameConfig.PlayAsLeika = false;
        }

        PlayAsIgorCheckBox.sprite = GameConfig.PlayAsIgor == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsJennyCheckBox.sprite = GameConfig.PlayAsJenny == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsLeikaCheckBox.sprite = GameConfig.PlayAsLeika == true ? CheckBoxChecked : CheckBoxUnchecked;
    }

    public void PlayAsIgor()
    {
        GameConfig.PlayAsIgor = !GameConfig.PlayAsIgor;

        if (GameConfig.PlayAsIgor == true)
        {
            GameConfig.PlayAsJenny = false;
            GameConfig.PlayAsLeika = false;
        }

        PlayAsIgorCheckBox.sprite = GameConfig.PlayAsIgor == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsJennyCheckBox.sprite = GameConfig.PlayAsJenny == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsLeikaCheckBox.sprite = GameConfig.PlayAsLeika == true ? CheckBoxChecked : CheckBoxUnchecked;
    }

    public void PlayAsLeika()
    {
        GameConfig.PlayAsLeika = !GameConfig.PlayAsLeika;

        if (GameConfig.PlayAsLeika == true)
        {
            GameConfig.PlayAsIgor = false;
            GameConfig.PlayAsJenny = false;
        }

        PlayAsIgorCheckBox.sprite = GameConfig.PlayAsIgor == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsJennyCheckBox.sprite = GameConfig.PlayAsJenny == true ? CheckBoxChecked : CheckBoxUnchecked;
        PlayAsLeikaCheckBox.sprite = GameConfig.PlayAsLeika == true ? CheckBoxChecked : CheckBoxUnchecked;
    }

    public void ResetPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();

        MessageBoxMenu.text = "Settings deleted - restart!";
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
